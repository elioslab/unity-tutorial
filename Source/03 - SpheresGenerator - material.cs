using UnityEngine;
using System.Collections;

public class SpheresGenerator : MonoBehaviour
{
	float x0;
	float y0;
	float z0;

	// Use this for initialization
	void Start()
	{
		GameObject playerObject = GameObject.Find("Player");

		x0 = playerObject.transform.position.x;
		y0 = playerObject.transform.position.y;
		z0 = playerObject.transform.position.z;

		StartCoroutine(createSphere());
		//A coroutine is a function that can suspend its execution
	}

	// Update is called once per frame
	void Update()
	{
	}

	IEnumerator  createSphere()
	{
		yield return new WaitForSeconds(0.3f);

		GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);

		sphere.AddComponent<Rigidbody>();

		float posX = Random.value;
		float posY = Random.value;
		float posZ = Random.value;	

		float coin = Random.value;
		if (coin < 0.5)
		{
			sphere.GetComponent<Renderer>().material.color = Color.green;
		}
		else
		{
			sphere.GetComponent<Renderer>().material.color = Color.red;
		}

		sphere.transform.position = new Vector3(x0 + posX, y0 + posY, z0 + posZ);

		StartCoroutine(removeSphere(sphere));

		StartCoroutine(createSphere());
	}

	IEnumerator  removeSphere(GameObject s)
	{
		yield return new WaitForSeconds(6);

		Destroy(s);
	}
}
