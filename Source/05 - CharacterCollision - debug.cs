using UnityEngine;
using System.Collections;

public class CharacterCollision : MonoBehaviour
{
	// Use this for initialization
	void Start ()
	{
	}

	// Update is called once per frame
	void Update ()
	{
	}

	void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.tag == "green")
		{
			Debug.Log("OK!");
			Destroy(collision.gameObject);
		}
		if (collision.gameObject.tag == "red")
		{
			Debug.Log("Noooo...");
			Destroy(collision.gameObject);
		}
	}
}

