using UnityEngine;
using System.Collections;

public class CharacterCollision : MonoBehaviour
{
	private int score = 0;

	public GUISkin gSkin;

	void OnGUI()
	{
		GUI.skin = gSkin;

		GUILayout.BeginArea(new Rect(10, 10, 100,100));
		GUILayout.Label(score.ToString());
		GUILayout.EndArea();
	}

	// Use this for initialization
	void Start ()
	{
	}

	// Update is called once per frame
	void Update ()
	{
	}

	void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.tag == "green")
		{
			Debug.Log("OK!");
			Destroy(collision.gameObject);
			score++;
		}
		if (collision.gameObject.tag == "red")
		{
			Debug.Log("Noooo...");
			Destroy(collision.gameObject);
			score--;
		}
	}
}