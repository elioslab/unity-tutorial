using UnityEngine;

public class SpheresGenerator : MonoBehaviour
{
	// Update is called once per frame
	void Update()
	{
		GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);

		sphere.AddComponent<Rigidbody>();

		float posX = Random.value;
		float posZ = Random.value;	
		sphere.transform.position = new Vector3(posX * 10, 15, posZ * 10);
	}
}